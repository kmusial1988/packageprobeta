package com.transporeon.proBeta.validationService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationPackage {


    public boolean valNamePackage(String name) {
        Pattern namePackage = Pattern.compile("^[a-zA-Z]");
        Matcher n = namePackage.matcher(name);
        return !name.isBlank() || n.find();
    }
    public boolean valSenderPackage(String name) {
        Pattern senderPackage = Pattern.compile("^[a-zA-Z]");
        Matcher n = senderPackage.matcher(name);
        return !name.isBlank() || n.find();
    }
    public boolean valRecipientPackage(String name) {
        Pattern recipientPackage = Pattern.compile("^[a-zA-Z]");
        Matcher n = recipientPackage.matcher(name);
        return !name.isBlank() || n.find();
    }
    public boolean valWeight(String size) {
        Pattern pSize1 = Pattern.compile("^[0-9]+.?[0-9]?");
        Matcher m = pSize1.matcher(size);

        return m.find();
    }

    public boolean valIdParcel(String id) {
        Pattern pIdParcel = Pattern.compile("^[A-Z]{3}[0-9]{2}$");
        Matcher m = pIdParcel.matcher(id);

        return m.find();
    }
    public boolean valUUid(String id) {
        Pattern pUUid = Pattern.compile("^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$");
        Matcher m = pUUid.matcher(id);

        return m.find();
    }


}
