package com.transporeon.proBeta.model;

public class ParcelModel {
    private String parcelId;
    private String name;
    private AddressModel address;

    public ParcelModel() {
    }

    public ParcelModel(String parcelId, String name, AddressModel address) {
        this.parcelId = parcelId;
        this.name = name;
        this.address = address;
    }

    public String getParcelId() {
        return parcelId;
    }

    public void setParcelId(String parcelId) {
        this.parcelId = parcelId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AddressModel getAddress() {
        return address;
    }

    public void setAddress(AddressModel address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "ParcelModel{" +
                "parcelId='" + parcelId + '\'' +
                ", name='" + name + '\'' +
                ", address=" + address +
                '}';
    }
}
