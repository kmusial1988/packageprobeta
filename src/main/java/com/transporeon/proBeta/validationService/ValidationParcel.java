package com.transporeon.proBeta.validationService;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ValidationParcel {

    public boolean valNameParcel(String name) {
        Pattern addressCityParcel = Pattern.compile("^[a-zA-Z]");
        Matcher n = addressCityParcel.matcher(name);
        return !name.isBlank() || n.find();
    }

    public boolean valIdParcel(String id) {
        Pattern pIdParcel = Pattern.compile("^[A-Z]{3}[0-9]{2}$");
        Matcher m = pIdParcel.matcher(id);

        return m.find();
    }

    public boolean valAddressStreet(String street){
        Pattern addressStreetParcel = Pattern.compile("^[a-zA-Z]");
                Matcher m = addressStreetParcel.matcher(street);


        return m.find();
    }
    public boolean valAddressPostalCode(String postalCode){
        Pattern addressPostalCodeParcel = Pattern.compile("^[0-9]{2}-[0-9]{3}$");
                Matcher o = addressPostalCodeParcel.matcher(postalCode);

        return o.find();
    }
    public boolean valAddressCity(String city){
                Pattern addressCityParcel = Pattern.compile("^[a-zA-Z]");
                        Matcher n = addressCityParcel.matcher(city);

        return n.find();
    }

}
