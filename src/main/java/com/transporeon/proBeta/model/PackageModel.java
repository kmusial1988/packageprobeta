package com.transporeon.proBeta.model;

import java.util.UUID;

public class PackageModel {

    private String packageId;
    private String name;
    private Size size;
    private String weight;
    private String sender;
    private ParcelModel senderParcelLocker;
    private String recipient;
    private ParcelModel recipientParcelLocker;
    private State state;

    public PackageModel() {
    }

    public PackageModel(String name, Size size, String weight, String sender,
                        ParcelModel senderParcelLocker, String recipient, ParcelModel recipientParcelLocker, State state) {
        String packageId = String.valueOf(UUID.randomUUID());


        this.packageId = packageId;
        this.name = name;
        this.size = size;
        this.weight = weight;
        this.sender = sender;
        this.senderParcelLocker = senderParcelLocker;
        this.recipient = recipient;
        this.recipientParcelLocker = recipientParcelLocker;
        this.state = state;
    }

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Size getSize() {
        return size;
    }

    public void setSize(Size size) {
        this.size = size;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public ParcelModel getSenderParcelLocker() {
        return senderParcelLocker;
    }

    public void setSenderParcelLocker(ParcelModel senderParcelLocker) {
        this.senderParcelLocker = senderParcelLocker;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public ParcelModel getRecipientParcelLocker() {
        return recipientParcelLocker;
    }

    public void setRecipientParcelLocker(ParcelModel recipientParcelLocker) {
        this.recipientParcelLocker = recipientParcelLocker;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    @Override
    public String toString() {
        return "PackageModel{" +
                "packageId=" + packageId +
                ", name='" + name + '\'' +
                ", size=" + size +
                ", weight=" + weight +
                ", sender='" + sender + '\'' +
                ", senderParcelLocker=" + senderParcelLocker +
                ", recipient='" + recipient + '\'' +
                ", recipientParcelLocker=" + recipientParcelLocker +
                ", state=" + state +
                '}';
    }

    public  enum Size {
        S("Small"),
        M("Medium"),
        L("Large"),
        XL("Extra Large");

        private String fullName;

        Size(String fullName) {
            this.fullName = fullName;
        }

        public String getFullName() {
            return fullName;
        }

    }

    public  enum State {
        PAC("Preparation package"),
        SEND("Sending to deliver"),
        DELIV("Deliver go to your parcel"),
        IN("Package in parcel");

        private String fullName;

        State(String fullName) {
            this.fullName = fullName;
        }

        public String getFullName() {
            return fullName;
        }

    }

}

