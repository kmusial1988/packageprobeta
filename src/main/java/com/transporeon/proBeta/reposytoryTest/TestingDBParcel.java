package com.transporeon.proBeta.reposytoryTest;

import com.transporeon.proBeta.model.AddressModel;
import com.transporeon.proBeta.model.ParcelModel;

import java.util.ArrayList;
import java.util.List;

public class TestingDBParcel {


    private List<ParcelModel> listParcel = new ArrayList<>();

    public TestingDBParcel() {

        this.listParcel.add(new ParcelModel("KRA01", "Szewska", new AddressModel("Szewska", "Krakow", "31-311")));

        this.listParcel.add(new ParcelModel("KRA02", "Opolska", new AddressModel("Opolska", "Krakow", "31-322")));

        this.listParcel.add(new ParcelModel("KRA03", "Bracka", new AddressModel("Bracka", "Krakow", "31-331")));

        this.listParcel.add(new ParcelModel("WAW01", "Grodzka", new AddressModel("Grodzka", "Warszawa", "41-351")));

        this.listParcel.add(new ParcelModel("WAA02", "Marka", new AddressModel("Marka", "Warszawa", "41-365")));

        this.listParcel.add(new ParcelModel("POZ01", "Dietla", new AddressModel("Dietla", "Poznan", "51-359")));

        this.listParcel.add(new ParcelModel("POZ02", "Dluga", new AddressModel("Dluga", "Poznan", "51-598")));


    }

    public List<ParcelModel> getListParcel() {
        return listParcel;
    }
}
