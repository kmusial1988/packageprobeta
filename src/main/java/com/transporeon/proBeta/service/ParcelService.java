package com.transporeon.proBeta.service;

import com.transporeon.proBeta.model.AddressModel;
import com.transporeon.proBeta.model.ParcelModel;
import com.transporeon.proBeta.reposytoryTest.TestingDBParcel;
import com.transporeon.proBeta.validationService.ValidationParcel;

import java.util.List;
import java.util.Scanner;

import static com.transporeon.proBeta.GUI.showMainMenu;

public class ParcelService {

    private static final TestingDBParcel testingDBParcel = new TestingDBParcel();

    private static final List<ParcelModel> parcelLockerDB = testingDBParcel.getListParcel();

    private static  ValidationParcel validation = new ValidationParcel();

    private static final Scanner scanner = new Scanner(System.in);


    public static void addParcel() {
        String id;
        do {
            System.out.println("Write id format [XXX00]:");
            id = scanner.nextLine();

            if (!validation.valIdParcel(id)) {
                System.out.println("Wrong data format try again");

            }
            if (checkParcelExist(id)) {
                System.out.println("Parcel is exist !!! ");
            }

        } while (!validation.valIdParcel(id) || checkParcelExist(id));

        String name;
        do {
            System.out.println("Write name :");
            name = scanner.nextLine();
            if (!validation.valNameParcel(name)) {
                System.out.println("You need write name !!!");
            }
        } while (!validation.valNameParcel(name));

        String addressStreet;
        do {
            System.out.println("Write address street :");
            addressStreet = scanner.nextLine();
            if (!validation.valAddressStreet(addressStreet)) {
                System.out.println("Wrong form, try again :");
            }
        } while (!validation.valAddressStreet(addressStreet));

        String addressCity;
        do {
            System.out.println("Write address city :");
            addressCity = scanner.nextLine();
            if (!validation.valAddressCity(addressCity)) {
                System.out.println("Wrong form, try again :");
            }

        }while (!validation.valAddressCity(addressCity));

        String addressPostalCode;
        do {
            System.out.println("Write address postal code example [00-000] : ");
            addressPostalCode = scanner.nextLine();
            if (!validation.valAddressPostalCode(addressPostalCode)) {
                System.out.println("Wrong form, try again :");
            }
        }while (!validation.valAddressPostalCode(addressPostalCode));

        System.out.println("----------------------");
        System.out.println("Adding status : " + parcelLockerDB.add(new ParcelModel(id, name, new AddressModel(addressStreet, addressCity, addressPostalCode))));
        System.out.println("----------------------");

        showMainMenu();

    }


    public static void removingParcel() {

        String removeParcelID;
        do {
            System.out.println("Write parcel locker Id to remove example [XXX00] : ");
            removeParcelID = scanner.nextLine();
            if(!validation.valIdParcel(removeParcelID)){
                System.out.println("You made a mistake try again  ...");
            }
            if(!checkParcelExist(removeParcelID)){
                System.out.println("Parcel is not exist !!!!");
            }

            for (int i = 0; i < parcelLockerDB.size(); i++) {


                if (parcelLockerDB.get(i).getParcelId().equals(removeParcelID)) {
                    parcelLockerDB.remove(i);
                    System.out.println("------------------------------");
                    System.out.println("Remove parcel locker " + removeParcelID);
                    System.out.println("------------------------------");
                    showMainMenu();
                }

            }
        }while (!validation.valIdParcel(removeParcelID) || !checkParcelExist(removeParcelID));
    }

    public static void displayAllParcel() {

        if (parcelLockerDB.size() == 0) {
            System.out.println("----------------------");
            System.out.println("List is empty !!!");
            System.out.println("----------------------");
            showMainMenu();
        } else {
            System.out.println("----------------------");
            for (ParcelModel listToDisplay : parcelLockerDB) {
                System.out.println(listToDisplay);
            }
            System.out.println("----------------------");
            showMainMenu();


        }
    }

    public static void displayAllParcelByCity() {
        String cityName;
        do {
            System.out.println("Write City where Parcel Locker are : ");
            cityName = scanner.next().toLowerCase();

            if(!validation.valAddressCity(cityName)){
                System.out.println("You made mistake try again !!!");
            }

            if(!parcelInCityExist(cityName)){
                System.out.println("There are no parcel lockers in this city !!!");
            }

            if (parcelLockerDB.size() == 0) {
                System.out.println("----------------------");
                System.out.println("List is empty !!!");
                System.out.println("----------------------");
                showMainMenu();
            } else {
                System.out.println("----------------------");
                for (int i = 0; i < parcelLockerDB.size(); i++) {
                    if (parcelLockerDB.get(i).getAddress().getCity().toLowerCase().equals(cityName)) {
                        System.out.println(parcelLockerDB.get(i));
                    }
                }
                System.out.println("----------------------");
                showMainMenu();


            }
        }while (!validation.valAddressCity(cityName) || !parcelInCityExist(cityName));
    }

    public static void updatingParcelName(ParcelModel parcelModel) {
        String newParcelName;
        do {
            System.out.println("Write new parcel locker name : ");
            newParcelName = scanner.next();
            if(!validation.valNameParcel(newParcelName)){
                System.out.println("You made mistake try again !!!");
            }
            for (int i = 0; i < parcelLockerDB.size(); i++) {
                if (parcelLockerDB.get(i).getName().equals(parcelModel.getName())) {
                    parcelLockerDB.get(i).setName(newParcelName);
                }
            }
            System.out.println("new parcel locker is " + newParcelName);

        }while (!validation.valNameParcel(newParcelName));

    }

    public static void updatingParcelAddress(ParcelModel parcelModel) {
        String newStreet;
        String newCity;
        String newPostelCode;
        do {
            System.out.println("Write new parcel Locker address street : ");
            newStreet = scanner.next();
            if(!validation.valAddressStreet(newStreet)){
                System.out.println("You made mistake try again !!!");
            }
            System.out.println("Write new parcel Locker address city : ");
            newCity = scanner.next();
            if(!validation.valAddressCity(newCity)){
                System.out.println("You made mistake try again !!!");
            }
            System.out.println("Write new parcel Locker address postal code format [00-000] : ");
            newPostelCode = scanner.next();
            if(!validation.valAddressPostalCode(newPostelCode)){
                System.out.println("You made mistake try again !!!");
            }
            for (int i = 0; i < parcelLockerDB.size(); i++) {
                if (parcelLockerDB.get(i).getName().equals(parcelModel.getName())) {
                    parcelLockerDB.get(i).setAddress(new AddressModel(newStreet, newCity, newPostelCode));
                }
                System.out.println("New Address is " + newStreet +", "+ newCity +", "+ newPostelCode);
            }
        }while (!validation.valAddressCity(newCity) || !validation.valAddressStreet(newStreet) || !validation.valAddressPostalCode(newPostelCode));

    }

    public static String parcelId() {
        String plid;
        do {
        System.out.println("Write parcel locker Id format [XXX00]");
        plid = scanner.next();

            if(!validation.valIdParcel(plid)){
                System.out.println("You made mistake try again !!!");
            }
            return plid;
        }while (!validation.valIdParcel(plid));
    }

    public static ParcelModel getParcelByID(String id) {

        ParcelModel result = new ParcelModel();

        String toCheck = id.toLowerCase();

        for (int i = 0; i < parcelLockerDB.size(); i++) {

            if (parcelLockerDB.get(i).getParcelId().toLowerCase().equals(toCheck)) {

                result.setName(parcelLockerDB.get(i).getName());
                result.setParcelId(parcelLockerDB.get(i).getParcelId());
                result.setAddress(parcelLockerDB.get(i).getAddress());

            }
        }
        return result;
    }


    public static ParcelModel getParcelByCity(String city) {

        ParcelModel result = new ParcelModel();

        String toCheck = city.toLowerCase();

        for (int i = 0; i < parcelLockerDB.size(); i++) {

            if (parcelLockerDB.get(i).getAddress().getCity().toLowerCase().equals(toCheck)) {

                result.setName(parcelLockerDB.get(i).getName());
                result.setParcelId(parcelLockerDB.get(i).getParcelId());
                result.setAddress(parcelLockerDB.get(i).getAddress());
            }
        }
        return result;
    }

    public static boolean checkParcelExist(String parcelId) {
        for (int i = 0; i < parcelLockerDB.size(); i++) {
            if (parcelLockerDB.get(i).getParcelId().equals(parcelId)) {
                return true;
            }
        }
        return false;
    }
    public static boolean parcelInCityExist(String nameCity){
        for (int i = 0; i<parcelLockerDB.size(); i++){
            if(parcelLockerDB.get(i).getAddress().getCity().toLowerCase().equals(nameCity)){
                return true;
            }
        }
        return false;
    }
}
