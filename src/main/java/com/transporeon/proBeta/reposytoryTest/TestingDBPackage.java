package com.transporeon.proBeta.reposytoryTest;

import com.transporeon.proBeta.model.PackageModel;

import java.util.ArrayList;
import java.util.List;

public class TestingDBPackage {

    private static final TestingDBParcel testingDBParcel = new TestingDBParcel();

    private List<PackageModel> listPackage = new ArrayList<>();


    public TestingDBPackage() {



        this.listPackage.add(new PackageModel("paczka4", PackageModel.Size.M, 54,
                "Lewandowska", testingDBParcel.getListParcel().get(1), "Glik", testingDBParcel.getListParcel().get(2), PackageModel.State.SEND));
        this.listPackage.add(new PackageModel("paczka1", PackageModel.Size.S,
                12, "Nowak", testingDBParcel.getListParcel().get(6), "Kowalski", testingDBParcel.getListParcel().get(4), PackageModel.State.PAC));
        this.listPackage.add(new PackageModel("paczka2", PackageModel.Size.L,
                15, "Jagielo", testingDBParcel.getListParcel().get(2), "Waza", testingDBParcel.getListParcel().get(5), PackageModel.State.IN));
        this.listPackage.add(new PackageModel("paczka3", PackageModel.Size.M,
                2, "Nowacek", testingDBParcel.getListParcel().get(2), "Kowalczyk", testingDBParcel.getListParcel().get(4), PackageModel.State.SEND));

    }


    public List<PackageModel> getListPackage() {
        return listPackage;
    }
}
