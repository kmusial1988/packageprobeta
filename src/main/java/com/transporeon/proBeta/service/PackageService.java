package com.transporeon.proBeta.service;

import com.transporeon.proBeta.model.PackageModel;
import com.transporeon.proBeta.model.ParcelModel;
import com.transporeon.proBeta.reposytoryTest.TestingDBPackage;
import com.transporeon.proBeta.validationService.ValidationPackage;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static com.transporeon.proBeta.GUI.showMainMenu;
import static com.transporeon.proBeta.service.ParcelService.*;

public class PackageService {

    private static final TestingDBPackage testingDBPackage = new TestingDBPackage();

    private static final List<PackageModel> packageDB = testingDBPackage.getListPackage();


    private static ValidationPackage validation = new ValidationPackage();

    private static final Scanner scanner = new Scanner(System.in);


    public static void addPackage() {
        String name;
        do {
            System.out.println("Write name :");
            name = scanner.nextLine();
            if (!validation.valNamePackage(name)) {
                System.out.println("Write correct name : ");
            }
        } while (!validation.valNamePackage(name));

        PackageModel.Size size;
        String sizePackage;
        boolean stateSize = false;
        do {
            System.out.println("Write size S/M/L/XL");
            sizePackage = scanner.next().toUpperCase();
            if (sizePackage.equals("S") || sizePackage.equals("M") || sizePackage.equals("L") || sizePackage.equals("XL")) {
                stateSize = true;
            }
            if (!stateSize) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!stateSize);
        size = PackageModel.Size.valueOf(sizePackage);

        String weight;
        do {
            System.out.println("Write weight format [00.00]:");
            weight = scanner.next();
            if (!validation.valWeight(weight)) {
                System.out.println("You made a mistake try again  ...");
            }
        }while (!validation.valWeight(weight));

        String sender;
        do {
            System.out.println("sender");
            sender = scanner.next();
            if (!validation.valSenderPackage(sender)) {
                System.out.println("Write correct sender : ");
            }
        } while (!validation.valSenderPackage(sender));

        ParcelModel senderParcel;
        String idParcel;
        do {
            System.out.println("Write sender parcel id [XXX00]:");
            idParcel = scanner.next();
            senderParcel = getParcelByID(idParcel);

            if (!ParcelService.checkParcelExist(idParcel)) {
                System.out.println("Parcel locker does not exist !!!");
            }
            if (!validation.valIdParcel(idParcel)) {
                System.out.println("Wrong data id parcel form [XXX00] :");
            }

        } while (!ParcelService.checkParcelExist(idParcel) || !validation.valIdParcel(idParcel));

        String recipient;
        do {
            System.out.println("Write recipient :");
            recipient = scanner.next();
            if (!validation.valRecipientPackage(recipient)) ;
        } while (!validation.valRecipientPackage(recipient));

        ParcelModel recipientParcel;
        String parcelIdRecipient;
        do {
            System.out.println("Write recipient parce id : ");
            parcelIdRecipient = scanner.next();
            recipientParcel = getParcelByID(parcelIdRecipient);
            if (!ParcelService.checkParcelExist(parcelIdRecipient)) {
                System.out.println("Parcel locker does not exist !!!");
            }
        } while (!validation.valIdParcel(parcelIdRecipient) || !ParcelService.checkParcelExist(parcelIdRecipient));

        String statePackage;
        boolean stateState = false;
        PackageModel.State state;
        do {
            System.out.println("Write state package PAC/SEND/DELIV/IN : ");
            statePackage = scanner.next().toUpperCase();
            if (statePackage.equals("PAC") || statePackage.equals("SEND") || statePackage.equals("DELIV") || statePackage.equals("IN")) {
                stateState = true;
            }
            if (!stateState) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!stateState);
        state = PackageModel.State.valueOf(statePackage);

        System.out.println("----------------------");
        System.out.println("Adding status is : " + packageDB.add(new PackageModel(name, size, weight, sender,
                senderParcel, recipient, recipientParcel, state)));
        System.out.println("----------------------");
        showMainMenu();


    }

    public static void removePackage() {
        String removePackageID;
        do {
            System.out.println("Write package Id to remove : ");
            removePackageID = scanner.nextLine();
            if (!validation.valUUid(removePackageID)) {
                System.out.println("You made a mistake try again  ...");
            }
            for (int i = 0; i < packageDB.size(); i++) {
                if (packageDB.get(i).getPackageId().equals(removePackageID)) {
                    packageDB.remove(i);
                    System.out.println("------------------------------");
                    System.out.println("Remove parcel locker " + removePackageID);
                    System.out.println("------------------------------");
                }

            }
        } while (!validation.valUUid(removePackageID));
    }

    public static void displayAllPackage() {
        if (packageDB.size() == 0) {
            System.out.println("----------------------");
            System.out.println("List is empty !!!");
            System.out.println("----------------------");
            showMainMenu();

        } else {
            System.out.println("----------------------");
            for (PackageModel listToDisplay : packageDB) {
                System.out.println(listToDisplay);
            }
            System.out.println("----------------------");
            showMainMenu();
        }
    }

    public static void displayAllPackageInParcel(String parcelId) {
        List<PackageModel> listPackageToDisplay = getPackageByParcelId(parcelId);
        if (listPackageToDisplay.size() == 0) {
            System.out.println("----------------------");
            System.out.println("List is empty !!!");
            System.out.println("----------------------");
            showMainMenu();
        } else {
            System.out.println("----------------------");
            for (int i = 0; i < listPackageToDisplay.size(); i++) {
                if (listPackageToDisplay.get(i).getSenderParcelLocker().getParcelId().equals(parcelId.toUpperCase())) {
                    System.out.println("Package to send : " + listPackageToDisplay.get(i));
                } else {
                    System.out.println("Package to reception : " + listPackageToDisplay.get(i));
                }
            }
            System.out.println("----------------------");
            showMainMenu();
        }
    }


    public static List<PackageModel> getPackageByParcelId(String ParcelId) {
        List<PackageModel> result = new ArrayList<>();
        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getRecipientParcelLocker().getParcelId().equals(ParcelId.toUpperCase())) {
                result.add(packageDB.get(i));
            }
            if (packageDB.get(i).getSenderParcelLocker().getParcelId().equals(ParcelId.toUpperCase())) {
                result.add(packageDB.get(i));
            }
        }
        return result;
    }

    public static void updatePackageName(PackageModel packageModel) {
        String newData;
        do {
            System.out.println("Write new package name : ");
            newData = scanner.next();
            if (!validation.valNamePackage(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
            for (int i = 0; i < packageDB.size(); i++) {
                if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                    packageDB.get(i).setName(newData);
                }
            }
            System.out.println("New package name in " + packageModel.getPackageId() + " is " + newData);
        } while (!validation.valNamePackage(newData));
    }

    public static void updatePackageSize(PackageModel packageModel) {
        String sizePackage;
        boolean stateSize = false;
        do {
            System.out.println("Write new package size S/M/L/XL : ");
            sizePackage = scanner.next().toUpperCase();
            if (sizePackage.equals("S") || sizePackage.equals("M") || sizePackage.equals("L") || sizePackage.equals("XL")) {
                stateSize = true;
            }
            if (!stateSize) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!stateSize);

        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setSize(PackageModel.Size.valueOf(sizePackage));
            }
        }
        System.out.println("New package size in " + packageModel.getPackageId() + " is " + packageModel.getSize().getFullName());
    }


    public static void updatePackageWeight(PackageModel packageModel) {
        String newData;
        do {
            System.out.println("Write new package weight : ");
            newData = scanner.next();
            if (!validation.valWeight(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!validation.valWeight(newData));

        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setWeight(newData);
            }
        }
        System.out.println("New package weight in " + packageModel.getPackageId() + " is " + newData);
    }

    public static void updatePackageSender(PackageModel packageModel) {
        String newData;
        do {
            System.out.println("Write new package Sender : ");
            newData = scanner.next();
            if (!validation.valSenderPackage(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!validation.valSenderPackage(newData));

        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setSender(newData);
            }
        }
        System.out.println("New package Sender in " + packageModel.getPackageId() + " is " + newData);
    }

    public static void updateParcelSender(PackageModel packageModel) {
        String newData;
        do {
            System.out.println("Write new parcel locker Id to Sender package: ");
            newData = scanner.next();
            if (!validation.valIdParcel(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!validation.valIdParcel(newData));
        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setSenderParcelLocker(ParcelService.getParcelByID(newData.toUpperCase()));
            }
        }
        System.out.println("New sender parcel locker with id: " + packageModel.getPackageId() + " is " + newData.toUpperCase());
    }

    public static void updatePackageRecipient(PackageModel packageModel) {
        String newData;
        do {
            System.out.println("Write new package Recipient : ");
            newData = scanner.next();
            if (!validation.valRecipientPackage(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!validation.valRecipientPackage(newData));
        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setRecipient(newData);
            }
        }
        System.out.println("New package Recipient in " + packageModel.getPackageId() + " is " + newData);
    }

    public static void updateParcelRecipient(PackageModel packageModel) {


        String newData;
        do {
            System.out.println("Write new parcel locker Id to Recipient package : ");
            newData = scanner.next();
            if (!validation.valIdParcel(newData)) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!validation.valIdParcel(newData));

        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setRecipientParcelLocker(ParcelService.getParcelByID(newData.toUpperCase()));
            }
        }
        System.out.println("New recipient parcel locker with id: " + packageModel.getPackageId() + " is " + newData.toUpperCase());
    }

    public static void updatePackageState(PackageModel packageModel) {
        String statePackage;
        boolean stateState = false;
        do {
            System.out.println("Write new package state PAC/SEND/DELIV/IN : ");
            statePackage = scanner.next().toUpperCase();
            if (statePackage.equals("PAC") || statePackage.equals("SEND") || statePackage.equals("DELIV") || statePackage.equals("IN")) {
                stateState = true;
            }
            if (!stateState) {
                System.out.println("You made a mistake try again  ...");
            }
        } while (!stateState);

        for (int i = 0; i < packageDB.size(); i++) {
            if (packageDB.get(i).getPackageId().equals(packageModel.getPackageId())) {
                packageDB.get(i).setState(PackageModel.State.valueOf(statePackage));
            }
        }
        System.out.println("New package size in " + packageModel.getPackageId() + " is " + packageModel.getState().getFullName());
    }

    public static PackageModel getPackageByID(String id) {

        PackageModel result = new PackageModel();

        String toCheck = id.toLowerCase();

        for (int i = 0; i < packageDB.size(); i++) {

            if (packageDB.get(i).getPackageId().toLowerCase().equals(toCheck)) {
                result.setPackageId(packageDB.get(i).getPackageId());
                result.setName(packageDB.get(i).getName());
                result.setSize(packageDB.get(i).getSize());
                result.setWeight(packageDB.get(i).getWeight());
                result.setRecipient(packageDB.get(i).getRecipient());
                result.setRecipientParcelLocker(packageDB.get(i).getRecipientParcelLocker());
                result.setSender(packageDB.get(i).getSender());
                result.setSenderParcelLocker(packageDB.get(i).getSenderParcelLocker());
                result.setState(packageDB.get(i).getState());
            }
        }
        return result;
    }

    public static String getPackageId() {
        String packageId;
        do {
            System.out.println("Write Package Id :");
            packageId = scanner.next();
            if (!validation.valUUid(packageId)) {
                System.out.println("You made a mistake try again  ...");
            }

        } while (!validation.valUUid(packageId));
        return packageId;

    }


}
