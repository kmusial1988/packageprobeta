package com.transporeon.proBeta;

import java.util.Scanner;

import static com.transporeon.proBeta.service.PackageService.*;
import static com.transporeon.proBeta.service.ParcelService.*;

public class GUI {

    private static final Scanner scanner = new Scanner(System.in);

    public static void showMainMenu() {

        String choose1;
        do{

            System.out.println("[1]. Adding parcel locker");
            System.out.println("[2]. Removing parcel locker");
            System.out.println("[3]. Displaying all parcel lockers");
            System.out.println("[4]. Displaying parcel lockers by city name");
            System.out.println("[5]. Updating parcel locker");
            System.out.println("[6]. Adding package");
            System.out.println("[7]. Removing package");
            System.out.println("[8]. Displaying packages by parcel locker");
            System.out.println("[9]. Updating package details");
            System.out.println("[0]. Exit");

            choose1 = scanner.nextLine();


            switch (choose1) {
                case "1":
                    addParcel();
                    break;
                case "2":
                    removingParcel();
                    break;
                case "3":
                    displayAllParcel();
                    break;
                case "4":
                    displayAllParcelByCity();
                    break;
                case "5":
                    updatingParcel();
                    break;
                case "6":
                    addPackage();
                    break;
                case "7":
                    removePackage();
                    break;
                case "8":
                    displayAllPackageInParcel(parcelId());
                    break;
                case "9":
                    updatingPackage();
                    break;
                case "0":
                    break;
                default:
                    System.out.println("Wrong choice !!");
                    break;
            }

        }while (!choose1.equals("0"));

    }
    public static void updatingParcel() {

        System.out.println("[1]. Update parcel locker name");
        System.out.println("[2]. Update parcel locker address");
        System.out.println("[0]. Back");

        String choose = scanner.nextLine();

        switch (choose) {
            case "1":
                updatingParcelName(getParcelByID(parcelId()));
                break;
            case "2":
                updatingParcelAddress(getParcelByID(parcelId()));
                break;
            case "0":
                showMainMenu();
                break;
            default:
                System.out.println("Wrong choice !!");
                updatingParcel();
                break;
        }

    }
    public static void updatingPackage() {
        System.out.println("[1]. Update package name");
        System.out.println("[2]. Update package size");
        System.out.println("[3]. Update package weight");
        System.out.println("[4]. Update package sender");
        System.out.println("[5]. Update sender parcel locker id");
        System.out.println("[6]. Update package recipient");
        System.out.println("[7]. Update recipient parcel locker id");
        System.out.println("[8]. Update package state");
        System.out.println("[0]. Back");

        String choose2 = scanner.nextLine();

        switch (choose2) {
            case "1":
                updatePackageName(getPackageByID(getPackageId()));
                break;
            case "2":
                updatePackageSize(getPackageByID(getPackageId()));
                break;
            case "3":
                updatePackageWeight(getPackageByID(getPackageId()));
                break;
            case "4":
                updatePackageSender(getPackageByID(getPackageId()));
                break;
            case "5":
                updateParcelSender(getPackageByID(getPackageId()));
                break;
            case "6":
                updatePackageRecipient(getPackageByID(getPackageId()));
                break;
            case "7":
                updateParcelRecipient(getPackageByID(getPackageId()));
                break;
            case "8":
                updatePackageState(getPackageByID(getPackageId()));
                break;

            case "0":
                showMainMenu();
                break;
            default:
                System.out.println("Wrong choice !!");
                updatingPackage();
                break;
        }
    }
}
